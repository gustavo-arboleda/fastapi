# Ejemplo de API con FastAPI

![https://docs.python.org/3/](https://img.shields.io/badge/Python-3.8%20%7C%203.9%20%7C%203.10-green?style=plastic&logo=python) ![https://fastapi.tiangolo.com/](https://img.shields.io/badge/FastAPI-0.89.1-green?style=plastic&logo=fastapi) ![https://pandas.pydata.org/docs/](https://img.shields.io/badge/Pandas-1.5.3-green?style=plastic&logo=pandas)

Desarrollado por @gustavo-arboleda, este es un proyecto que implementa una API de creación y venta de productos construido en Python usando el framework FastAPI, Pandas Dataframe para la manipulación de datos y archivos con formato Json para la persistencia de la información.

## Descripción

Este es un proyecto ha sido creado con el propósito de formar a todas las personas interesadas en aprender a crear APIs con Python y FastAPI. Por lo anterior, este proyecto no está dockerizado ni tiene persistencia en base de datos, la presistencia de este proyecto se da mediante estructuras de dataframes en archivos con formato JSON.

## Guía de instalación

### Requerimientos

- Python 3.8 o superior
- Git
- PIP

### Pasos para la instalación

1. Clonar el repositorio en el directorio de preferencia ejecutando el comando:  
   `git clone https://gitlab.com/gustavo-arboleda/fastapi.git`

2. Posicionarse en el directorio del proyecto:  
   `cd fastapi`

3. Crear un entorno virtual:  
   Para Ubuntu (linux): `python3 -m venv environment`

4. Activar el entorno virtual:  
   Para Ubuntu (linux): `source environment/bin/activate`

5. Instalar las dependencias necesarias para trabajar proyecto:  
   `pip install -r requirements.txt`

6. Ejecutar el servidor para trabajar con la API:  
   `uvicorn src.main:app --reload --workers 1 --host 0.0.0.0 --port 8000`

7. Ver la API en `http://localhost:8000/dev-docs`

## Documentación Oficial

- Documentación para Python: [https://docs.python.org/3/](https://docs.python.org/3/)
- Documentación para FastAPI: [https://fastapi.tiangolo.com/](https://fastapi.tiangolo.com/)
- Documentación para Pandas: [https://pandas.pydata.org/docs/](https://pandas.pydata.org/docs/)

## Imágenes del proyecto

### En la terminal:

![Terminal](/images/1.png)

### En el navegador:

![Navegador](/images/2.png)

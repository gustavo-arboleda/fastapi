from fastapi import FastAPI

from .api import ping_routes, products_routes, sales_routes

app = FastAPI(title="FastAPI Example", docs_url="/dev-docs")
app.include_router(ping_routes.router, prefix="/ping", tags=["Ping"])
app.include_router(products_routes.router, prefix="/api/v1/products", tags=["Products"])
app.include_router(sales_routes.router, prefix="/api/v1/sales", tags=["Sales"])

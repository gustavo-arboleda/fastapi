from pydantic import BaseModel, Field


class ProductIn(BaseModel):
    name: str = Field(...)
    reference: str = Field(...)
    price: float = Field(...)
    stock: int = Field(..., gt=0)

    class Config:
        schema_extra = {
            "example": {
                "name": "Funko pop - Asoka Tano",
                "reference": "funko-1",
                "price": 50000,
                "stock": 1,
            }
        }


class ProductOut(BaseModel):
    id: int
    name: str
    reference: str
    price: float
    stock: int
    created_at: str
    updated_at: str

from datetime import datetime
from typing import Union

import pandas as pd

file_path = "./src/dao/products/persistence.json"


def create_product(
    id: int,
    name: str,
    reference: str,
    price: float,
    stock: int,
) -> bool:
    """
    ## Create a record in the product's json file
    """
    # Get the current data
    global file_path
    try:
        df = pd.read_json(file_path, orient="table", convert_dates=False)
    except ValueError:
        df = pd.DataFrame(
            columns=[
                "id",
                "created_at",
                "updated_at",
                "name",
                "reference",
                "price",
                "stock",
            ]
        )

    # Create the new record
    current_date = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
    new_product = {
        "id": [id],
        "created_at": [current_date],
        "updated_at": [current_date],
        "name": [name],
        "reference": [reference],
        "price": [price],
        "stock": [stock],
    }

    # Concat the new record with the current data
    df_new_product = pd.DataFrame.from_dict(new_product)
    df = pd.concat([df, df_new_product])
    df.to_json(file_path, orient="table", index=False)
    return True


def get_product(
    id: Union[int, None],
    name: Union[str, None],
    reference: Union[str, None],
    price: Union[float, None],
    stock: Union[int, None],
) -> dict:
    """
    ## Filter records from the product's json file
    """
    global file_path
    try:
        df = pd.read_json(file_path, orient="table", convert_dates=False)
    except ValueError:
        return []
    if id:
        df = df.loc[df["id"] == id]
    if name:
        df = df.loc[df["name"].str.contains(name, case=False)]
    if reference:
        df = df.loc[df["reference"].str.contains(name, case=False)]
    if price:
        df = df.loc[df["price"] == price]
    if stock:
        df = df.loc[df["stock"] == stock]
    return df.to_dict(orient="records")


def update_product(
    id: int,
    name: Union[str, None],
    reference: Union[str, None],
    price: Union[float, None],
    stock: Union[int, None],
) -> bool:
    """
    ## Update a record in the product's json file
    """
    global file_path
    df = pd.read_json(file_path, orient="table", convert_dates=False)
    row_index = df.loc[df["id"] == id].index.values[0]

    if name:
        df.at[row_index, "name"] = name
    if reference:
        df.at[row_index, "reference"] = reference
    if price:
        df.at[row_index, "price"] = price
    if stock:
        df.at[row_index, "stock"] = stock

    if name or reference or price or stock:
        current_date = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
        df.at[row_index, "updated_at"] = current_date

    df.to_json(file_path, orient="table", index=False)
    return True


def delete_product(id: int) -> bool:
    """
    ## Delete a record from the product's json file
    """
    global file_path
    df = pd.read_json(file_path, orient="table", convert_dates=False)
    row_index = df.loc[df["id"] == id].index.values[0]
    df.drop(row_index, inplace=True)
    df.to_json(file_path, orient="table", index=False)
    return True


def reference_already_exists(reference: str) -> bool:
    """
    ## Recieves a reference and validate if it exists in the persistence
    - Returns True if the reference passed already exists elsewise returns False
    """
    try:
        global file_path
        df = pd.read_json(file_path, orient="table")
        reference_exist = df.loc[df["reference"] == reference]
        return not reference_exist.empty
    except ValueError:
        return False


def id_already_exists(id: int) -> bool:
    """
    ## Recieves an id and validate if it exists in the persistence
    - Returns True if the id passed already exists elsewise returns False
    """
    try:
        global file_path
        df = pd.read_json(file_path, orient="table")
        id_exists = df.loc[df["id"] == id]
        return not id_exists.empty
    except ValueError:
        return False

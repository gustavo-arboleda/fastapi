from datetime import datetime
from typing import List, Union

import pandas as pd

from .schema import SaleIn

sales_file_path = "./src/dao/sales/persistence.json"
products_file_path = "./src/dao/products/persistence.json"


def create_sale(
    id: int,
    reference: str,
    sale_info: List[SaleIn],
) -> bool:
    """
    ## Create a record in the sale's json file
    """
    # Get the current data
    global sales_file_path
    try:
        sales_df = pd.read_json(sales_file_path, orient="table", convert_dates=False)
    except ValueError:
        sales_df = pd.DataFrame(
            columns=["id", "created_at", "updated_at", "reference", "items", "total"]
        )
    global products_file_path
    products_df = pd.read_json(products_file_path, orient="table", convert_dates=False)

    # Create the new record
    current_date = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
    new_sale = {
        "id": [id],
        "created_at": [current_date],
        "updated_at": [current_date],
        "reference": [reference],
    }
    items = []
    total = float(0)
    for sale in sale_info:
        product_row = products_df.loc[products_df["id"] == sale.product_id]
        price = product_row["price"].values[0]
        total += float(price * sale.quantity)
        item = {
            "product_id": sale.product_id,
            "price": price,
            "subtotal": price * sale.quantity,
            "quantity": sale.quantity,
        }
        items.append(item)
    new_sale["items"] = [items]
    new_sale["total"] = [total]

    # Concat the new record with the current data
    df_new_sale = pd.DataFrame.from_dict(new_sale)
    sales_df = pd.concat([sales_df, df_new_sale])
    sales_df.to_json(sales_file_path, orient="table", index=False)
    return True


def get_sale(
    id: Union[int, None],
    product_id: Union[int, None],
    reference: Union[str, None],
) -> dict:
    """
    ## Filter records from the sale's json file
    """
    global sales_file_path
    try:
        df = pd.read_json(sales_file_path, orient="table", convert_dates=False)
    except ValueError:
        return []
    if id:
        df = df.loc[df["id"] == id]
    if reference:
        df = df.loc[df["reference"].str.contains(reference, case=False)]
    if product_id:
        df = df[
            df["items"].apply(
                lambda x: product_id in [item["product_id"] for item in x]
            )
        ]
    return df.to_dict(orient="records")


def delete_sale(id: int) -> bool:
    """
    ## Delete a record from the sale's json file
    """
    global sales_file_path
    df = pd.read_json(sales_file_path, orient="table", convert_dates=False)
    row_index = df.loc[df["id"] == id].index.values[0]
    df.drop(row_index, inplace=True)
    df.to_json(sales_file_path, orient="table", index=False)
    return True


def reference_already_exists(reference: str) -> bool:
    """
    ## Recieves a reference and validate if it exists in the persistence
    - Returns True if the reference passed already exists elsewise returns False
    """
    try:
        global sales_file_path
        df = pd.read_json(sales_file_path, orient="table")
        reference_exist = df.loc[df["reference"] == reference]
        return not reference_exist.empty
    except ValueError:
        return False


def id_already_exists(id: int) -> bool:
    """
    ## Recieves an id and validate if it exists in the persistence
    - Returns True if the id passed already exists elsewise returns False
    """
    global sales_file_path
    try:
        df = pd.read_json(sales_file_path, orient="table")
        id_exists = df.loc[df["id"] == id]
        return not id_exists.empty
    except ValueError:
        return False

from typing import List

from pydantic import BaseModel, Field


class SaleIn(BaseModel):
    product_id: int = Field(...)
    quantity: int = Field(..., gt=0)

    class Config:
        schema_extra = {
            "example": {
                "product_id": 1,
                "quantity": 2,
            }
        }


class SaleOutDetail(BaseModel):
    product_id: int
    price: float
    quantity: int
    subtotal: float


class SaleOut(BaseModel):
    id: int
    created_at: str
    updated_at: str
    reference: str
    items: List[SaleOutDetail]
    total: float

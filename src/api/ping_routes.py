from fastapi import APIRouter


router = APIRouter()


@router.get("/")
def ping():
    """
    # Validate the API state
    ## Returns:
        {"is the server ok?": True}
    """
    return {"is the server ok?": True}

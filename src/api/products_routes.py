from typing import List

from fastapi import APIRouter, Body, HTTPException, Path, Query, status

from ..dao.products.crud import create_product as dao_create_product
from ..dao.products.crud import delete_product as dao_delete_product
from ..dao.products.crud import get_product as dao_get_product
from ..dao.products.crud import id_already_exists, reference_already_exists
from ..dao.products.crud import update_product as dao_update_product
from ..dao.products.schema import ProductIn, ProductOut

router = APIRouter()


@router.post("/{id}", status_code=status.HTTP_201_CREATED)
def create_a_product(
    id: int = Path(..., gt=0, description="Id for product will create", example=1),
    product: ProductIn = Body(...),
):
    """
    # Create a product

    Receives a product id and a body with the product information and create it in the persistence

    ### Params:
        - id (Path): int -> Id for product will create
        - product: ProductIn -> product information

    ### Returns:
        - HTTP_201_CREATED: null
    """
    try:
        # Validate if the product id already exists
        assert id_already_exists(id) is False, f"Id '{id}' already exist"
        # Validate if the product reference already exists
        assert (
            reference_already_exists(product.reference) is False
        ), f"Reference '{product.reference}' already exist"

        # Create the product
        dao_create_product(
            id=id,
            name=product.name,
            reference=product.reference,
            price=product.price,
            stock=product.stock,
        )
    except AssertionError as ex:
        err_msg = f"Validation error creating a product: {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=err_msg
        )
    except Exception as ex:
        err_msg = f"Unexpected error creating a product: {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=err_msg
        )


@router.get("/", status_code=status.HTTP_200_OK, response_model=List[ProductOut])
def get_products(
    id: int = Query(None, gt=0, description="Filter by product id"),
    name: str = Query(None, description="Filter by product name", example="Asoka"),
    reference: str = Query(None, description="Filter by product reference"),
    price: float = Query(None, description="Filter by product price"),
    stock: int = Query(None, gt=0, description="Filter by product stock"),
):
    """
    # Get muliple products with filters

    Can recieve id, name, reference, price or stock as query params to filter the products
    in the persistence

    ### Params:
        - id (Query): int -> Filter by product id
        - name (Query): str -> Filter by product name
        - reference (Query): str -> Filter by product reference
        - price (Query): float -> Filter by product price
        - stock (Query): int -> Filter by product stock

    ### Returns:
        - List[ProductOut] -> Products List
    """
    try:
        # Filter the products
        return dao_get_product(
            id=id,
            name=name,
            reference=reference,
            price=price,
            stock=stock,
        )
    except Exception as ex:
        err_msg = f"Unexpected error filtering products: {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=err_msg
        )


@router.put("/{id}", status_code=status.HTTP_200_OK)
def update_a_product(
    id: int = Path(..., gt=0, description="Id for product will update", example=1),
    name: str = Query(
        None,
        description="New name for the product",
        example="Funko pop - Asoka Tano - Mandalorian",
    ),
    reference: str = Query(
        None, description="New reference for the product", example="funko-mando-1"
    ),
    price: float = Query(None, description="New price for the product", example=70000),
    stock: int = Query(None, gt=0, description="New stock for the product", example=5),
):
    """
    # Update a product partially in the persistence

    Receives a product id and a body with the product information and update it in the persistence

    ### Params:
        - id (Path): int -> Id for product will update
        - name (Query): str -> Filter by product name
        - reference (Query): str -> Filter by product reference
        - price (Query): float -> Filter by product price
        - stock (Query): int -> Filter by product stock

    ### Returns:
        - HTTP_200_OK: null
    """
    try:
        # Validate if the product id exists
        assert id_already_exists(id), f"Id '{id}' does not exist"

        # Update the product
        dao_update_product(
            id=id,
            name=name,
            reference=reference,
            price=price,
            stock=stock,
        )
    except AssertionError as ex:
        err_msg = f"Validation error updating the product: {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=err_msg
        )
    except Exception as ex:
        err_msg = f"Unexpected error updating the product id '{id}': {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=err_msg
        )


@router.delete("/{id}", status_code=status.HTTP_200_OK)
def delete_a_product(
    id: int = Path(..., gt=0, description="Id for product will delete", example=1),
):
    """
    # Delete a product from the persistence

    Receives a product id and delete it from the persistence

    ### Params:
        - id (Path): int -> Id for product will delete

    ### Returns:
        - HTTP_200_OK: null
    """
    try:
        # Validate if the product id exists
        assert id_already_exists(id), f"Id '{id}' does not exist"

        # Delete the product from the persistence
        dao_delete_product(
            id=id,
        )
    except AssertionError as ex:
        err_msg = f"Validation error deleting the product: {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=err_msg
        )
    except Exception as ex:
        err_msg = f"Unexpected error deleting the product id '{id}': {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=err_msg
        )

from typing import List

from fastapi import APIRouter, HTTPException, Path, Query, status, Body

from ..dao.products.crud import id_already_exists as product_id_already_exists
from ..dao.sales.crud import create_sale as dao_create_sale
from ..dao.sales.crud import delete_sale as dao_delete_sale
from ..dao.sales.crud import get_sale as dao_get_sale
from ..dao.sales.crud import id_already_exists as sale_id_already_exists

from ..dao.sales.crud import reference_already_exists
from ..dao.sales.schema import SaleOut, SaleIn

router = APIRouter()


@router.post("/{id}", status_code=status.HTTP_201_CREATED)
def create_a_sale(
    id: int = Path(..., gt=0, description="Id for sale will create", example=1),
    reference: str = Query(
        ..., description="Reference for product will create", example="sale-1"
    ),
    sale_info: List[SaleIn] = Body(
        ...,
        description="Sale detail",
        example=[
            {
                "product_id": 1,
                "quantity": 2,
            },
            {
                "product_id": 2,
                "quantity": 1,
            },
        ],
    ),
):
    """
    # Create a sale

    Receives a sale id, a sale reference and a body with the sale detail and create it in the
    persistence

    ### Params:
        - id (Path): int -> Id for sale will create
        - reference (Query): str -> Reference for product will create
        - sale_info: saleIn -> Sale detail

    ### Returns:
        - HTTP_201_CREATED: null
    """
    try:
        # Validate if the sale id already exists
        assert sale_id_already_exists(id) is False, f"Id '{id}' already exist"

        # Validate if the product reference already exists
        assert (
            reference_already_exists(reference) is False
        ), f"Reference '{reference}' already exist"

        for sale in sale_info:
            # Validate if the product id exists
            assert product_id_already_exists(
                sale.product_id
            ), f"Product id '{sale.product_id}' does not exist"

        # Create the sale
        dao_create_sale(
            id=id,
            reference=reference,
            sale_info=sale_info,
        )
    except AssertionError as ex:
        err_msg = f"Validation error creating a sale: {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=err_msg
        )
    except Exception as ex:
        err_msg = f"Unexpected error creating a sale: {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=err_msg
        )


@router.get("/", status_code=status.HTTP_200_OK, response_model=List[SaleOut])
def get_sales(
    id: int = Query(None, gt=0, description="Filter by sale id"),
    product_id: int = Query(None, gt=0, description="Filter by product id"),
    reference: str = Query(
        None, description="Filter by sale reference", example="sale-1"
    ),
):
    """
    # Get muliple sales with filters

    Can recieve id, product_id, reference as query params to filter sales in the
    persistence

    ### Params:
        - id (Query): int -> Filter by sale id
        - product_id (Query): str -> Filter by product id
        - reference (Query): str -> Filter by sale reference

    ### Returns:
        - List[saleOut] -> Sales List
    """
    try:
        # Filter the sales
        return dao_get_sale(
            id=id,
            product_id=product_id,
            reference=reference,
        )
    except Exception as ex:
        err_msg = f"Unexpected error filtering sales: {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=err_msg
        )


@router.delete("/{id}", status_code=status.HTTP_200_OK)
def delete_a_sale(
    id: int = Path(..., gt=0, description="Id for product will delete", example=1),
):
    """
    # Delete a sale from the persistence

    Receives a sale id and delete it from the persistence

    ### Params:
        - id (Path): int -> Id for product will delete

    ### Returns:
        - HTTP_200_OK: null
    """
    try:
        # Validate if the sale id already exists
        assert sale_id_already_exists(id), f"Id '{id}' does not exist"

        # Delete the sale from the persistence
        dao_delete_sale(
            id=id,
        )
    except AssertionError as ex:
        err_msg = f"Validation error deleting the sale: {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=err_msg
        )
    except Exception as ex:
        err_msg = f"Unexpected error deleting the sale id '{id}': {ex}"
        print("Debug:", err_msg)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=err_msg
        )
